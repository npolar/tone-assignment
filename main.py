 1   import random
    1 import socket
    2 import time
    3 import hashlib
    4 import json
    5 
    6 def generate_weather_data():
    7 ▏   temperature = round(random.uniform(10, 30), 2)
    8 ▏   humidity = round(random.uniform(30, 80), 2)
    9 ▏   pressure = round(random.uniform(980, 1050), 2)
   10 ▏   wind_speed = round(random.uniform(0, 20), 2)
   11 ▏   rainfall = round(random.uniform(0, 10), 2)
   12 ▏   
   13 ▏   weather_data = {
   14 ▏   ▏   "Temperature (deg C)": temperature,
   15 ▏   ▏   "Humidity (%)": humidity,
   16 ▏   ▏   "Pressure (hPa)": pressure,
   17 ▏   ▏   "Wind Speed (m/s)": wind_speed,
   18 ▏   ▏   "Rainfall (mm)": rainfall,
   19 ▏   }
   20 ▏   
   21 ▏   # Calculate checksum using MD5 hash
   22 ▏   data_json = json.dumps(weather_data, sort_keys=True)
   23 ▏   checksum = hashlib.md5(data_json.encode()).hexdigest()
   24 ▏   
   25 ▏   # Add the checksum to the weather data
   26 ▏   weather_data["Checksum"] = checksum
   27 ▏   
   28 ▏   return weather_data
   29 
   30 def publish_weather_data_udp(host, port):
   31 ▏   udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
   32 ▏   
   33 ▏   while True:
   34 ▏   ▏   weather_data = generate_weather_data()
   35 ▏   ▏   data_json = json.dumps(weather_data)
   36 ▏   ▏   udp_socket.sendto(data_json.encode(), (host, port))
   37 ▏   ▏   print(f"Published: {data_json}")
▎  38 ▏   ▏   time.sleep(1)  # Adjust the sleep interval as needed
   39 ▏   ▏   
   40 if __name__ == "__main__":
   41 ▏   udp_host = "127.0.0.1"
   42 ▏   udp_port = 12345
   
   44 ▏   print("Weather Station Simulation Started:")
   45 ▏   publish_weather_data_udp(udp_host, udp_port)
~
~
